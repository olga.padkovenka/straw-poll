<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Reponse;
use App\Form\QuestionType;
use App\Form\QuestionVoteType;
use App\Repository\QuestionRepository;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $question = new Question();
        $question->addReponse(new Reponse());
        $question->addReponse(new Reponse());
        $question->addReponse(new Reponse());
        $form = $this->createForm(QuestionType::class, $question);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $question = $form->getData();
            $reponses = $question->getReponses(); //je récupère toutes les répopnses

            foreach ($reponses as $reponse) {
                if (!is_null($reponse->getReponse())) {
                    $entityManager->persist($reponse);
                } else {
                    $question->removeReponse($reponse); //si la réponse n'est pas nulle, on fait "persist", sinon, on surpime la question.
                }
            }

            $entityManager->persist($question);
            $entityManager->flush();

            return $this->redirectToRoute("question", [
                "id" => $question->getId()
            ]);
        }

        return $this->render("index.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/question/{id}", name="question")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function question(int $id, Request $request): Response
    {
        $question = $this->getDoctrine()
            ->getRepository(Question::class)->find($id); //on récurère la question
        $form = $this->createForm(QuestionVoteType::class, $question);

        return $this->render("question.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/question/{id}/{voteIndex}", name="vote")
     * @param int $id
     * @param int $voteIndex
     * @param Request $request
     * @return Response
     */
    public function vote(int $id, int $voteIndex, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $question = $this->getDoctrine()
            ->getRepository(Question::class)->find($id);
        $reponse = $question->getReponse($voteIndex);
        $reponse->setVote($reponse->getVote() + 1);

        $entityManager->persist($reponse);
        $entityManager->persist($question);
        $entityManager->flush();

        return $this->json([
            "reponse" => $reponse->getReponse()
        ]);
    }
}
